/**
* Listen for clicks on the buttons, and send the appropriate message to
* the content script in the page.
*/
document.addEventListener("click", (e) => {
	function applyChange(tabs) {
		browser.tabs.sendMessage(tabs[0].id, {
			command: e.target.id
		});
	}
	browser.tabs.query({active: true, currentWindow: true})
		.then(applyChange);
});
