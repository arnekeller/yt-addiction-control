// inject CSS

const STYLE_CLASS = "youtube-ac-styling";

Array.from(document.getElementsByClassName(STYLE_CLASS)).forEach((e) => {
    e.remove();
});

window.ytacCSS = {
	"sidebar": "#related",
	"comments": "#comments, #comment-teaser",
	"endscreen": ".videowall-endscreen",
	"invideo": ".ytp-ce-video",
	"notification": "ytd-notification-topbar-button-renderer",
	"other": "#newness-dot, .ytp-spinner, .ytp-next-button"
};

const INVISIBLE = " { display: none !important; }";

function addCSS(key) {
	if (document.getElementById("ytac" + key) !== null) {
		return;
	}
	let head = document.getElementsByTagName('head')[0];
	let e = document.createElement('style');
	e.setAttribute('type', 'text/css');
	e.setAttribute("class", STYLE_CLASS);
	e.setAttribute("id", "ytac" + key);

	if ('textContent' in e) {
		e.textContent = window.ytacCSS[key] + INVISIBLE;
	} else {
		e.styleSheet.cssText = window.ytacCSS[key] + INVISIBLE;
	}

	head.appendChild(e);
}

function addClickListeners(key) {
	for (el of document.querySelectorAll(window.ytacCSS[key])) {
		el.addEventListener("click", (e) => addCSS(key));
	}
}

for (key of Object.keys(window.ytacCSS)) {
	addCSS(key);
}

// browser action listener
  
// Listen for messages from the background script.
if (window.ytacListener != null) {
	browser.runtime.onMessage.removeListener(window.ytacListener);
}
window.ytacListener = (message) => {
	console.log("[YTAC] received browser action " + message.command);
  	if (message.command == "comments") {
		console.log("[YTAC] showing comments");
		document.getElementById("ytaccomments").remove();
  	} else if (message.command == "sidebar") {
		console.log("[YTAC] showing video sidebar");
		document.getElementById("ytacsidebar").remove();
		addClickListeners("sidebar");
	} else if (message.command == "invideo") {
		console.log("[YTAC] showing in-video suggestions");
		document.getElementById("ytacinvideo").remove();
		addClickListeners("invideo");
	} else if (message.command == "endscreen") {
		console.log("[YTAC] showing endscreen");
		document.getElementById("ytacendscreen").remove();
		addClickListeners("endscreen");
	} else if (message.command == "notification") {
		console.log("[YTAC] showing notification bell");
		document.getElementById("ytacnotification").remove();
		addClickListeners("notification");
	}
};
browser.runtime.onMessage.addListener(window.ytacListener);
console.log("[YTAC] added action listener");

// expand description by default
let showMoreButton = document.querySelector("#description-inline-expander > #expand");
if (showMoreButton) {
	showMoreButton.click();
}

// attempt to hide the notification count in the title

function hideNotificationCount() {
	let a = document.getElementsByTagName("title")[0];
	if (a.innerText.match("^\\(\\d+\\).*")) {
		a.innerText = a.innerText.substring(a.innerText.indexOf(")") + 2);
	}
}
setInterval(hideNotificationCount, 1000);
